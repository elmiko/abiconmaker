class_name Pixel
extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var h = ReferenceRect.new()
	h.rect_size = Vector2(32, 32)
	h.border_color = Color("ffffff")
	h.border_width = 2
	h.editor_only = true
	h.mouse_filter = h.MOUSE_FILTER_IGNORE
	add_child(h)
	connect("mouse_entered", self, "_enable_highlight")
	connect("mouse_exited", self, "_disable_highlight")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func set_color(color):
	self.color = color


func _enable_highlight():
	var h = get_child(0)
	h.editor_only = false


func _disable_highlight():
	var h = get_child(0)
	h.editor_only = true
