extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Number of pixels per side in the output image
const ICON_SIZE = 16
# Number of pixels per side on the icon rects in the editor
const ICON_RECT_SIZE = 32
# colors
const PIXEL_TRANSPARENT = Color("#00000000")

var _current_color = Color("#ffffffff")

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(ICON_SIZE * ICON_SIZE):
		var pixel = Pixel.new()
		pixel.color = PIXEL_TRANSPARENT
		pixel.rect_size = Vector2(ICON_RECT_SIZE, ICON_RECT_SIZE)
		var x = (i % ICON_SIZE) * ICON_RECT_SIZE
		var y = int(i / ICON_SIZE) * ICON_RECT_SIZE
		pixel.rect_position = Vector2(x, y)
		pixel.name = "Pixel" + str(i)
		pixel.mouse_filter = pixel.MOUSE_FILTER_PASS
		$PixelCanvas.add_child(pixel)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_PixelCanvas_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			var p = int(event.position[0] / ICON_RECT_SIZE) + (int(event.position[1] / ICON_RECT_SIZE) * ICON_SIZE)
			var pixel = $PixelCanvas.get_child(p)
			if event.button_index == 1:
				$Palette.add_color(_current_color)
				pixel.set_color(_current_color)
			else:
				pixel.set_color(PIXEL_TRANSPARENT)




func _on_ColorPicker_color_changed(color):
	_current_color = color


func _on_ClearButton_pressed():
	for p in $PixelCanvas.get_children():
		p.set_color(PIXEL_TRANSPARENT)


func _on_SaveButton_pressed():
	$FileDialog. popup_centered()


func _on_FileDialog_file_selected(path):
	var img = Image.new()
	img.create(16, 16, false, Image.FORMAT_RGBA8)
	img.lock()
	for p in $PixelCanvas.get_children():
		var x = int(p.rect_position[0] / ICON_RECT_SIZE)
		var y = int(p.rect_position[1] / ICON_RECT_SIZE)
		img.set_pixel(x, y, p.color)
	img.save_png(path)
