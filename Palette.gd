extends Control

const MAX_PALETTES = 7
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var index = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func add_color(color):
	for c in get_children():
		if c.color == color:
			return
	if index == 7:
		index = 0
	var c = get_child(index)
	c.color = color
	c.show()
	index += 1
